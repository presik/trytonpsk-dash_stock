# This file is part of purchase_discount module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval
from trytond.modules.dash.dash import DashAppBase


class ShipmentInternal(metaclass=PoolMeta):
    __name__ = 'stock.shipment.internal'
    requested_by = fields.Many2One(
        'company.employee', 'Requested by',
        domain=[('company', '=', Eval('company', -1))],
        states={
            'readonly': Eval('state').in_(['done', 'cancel']),
            },
        depends=['company', 'state'])

    @classmethod
    def __setup__(cls):
        super(ShipmentInternal, cls).__setup__()

    @classmethod
    def dash_wait(cls, args, ctx):
        for v in args['moves']:
            del v['id']
            v['quantity'] = int(v['quantity'])
            v['company'] = ctx['company']
            v['from_location'] = args['from_location']['id']
            v['to_location'] = args['to_location']['id']

        to_create = {
            'company': ctx['company'],
            'from_location': args['from_location']['id'],
            'to_location': args['to_location']['id'],
            'state': 'request',
            'effective_date': args['effective_date'],
            'requested_by': args['requested_by']['id'],
            'moves': [('create', args['moves'])],
        }
        shipment, = cls.create([to_create])
        record = args.copy()
        moves = []
        for m in shipment.moves:
            moves.append({
                'id': m.id,
                'quantity': m.quantity,
                'uom': {
                    'id': m.uom.id,
                    'name': m.uom.rec_name,
                },
                'product': {
                    'id': m.product.id,
                    'name': m.product.name
                },
            })
        record.update({
            'id': shipment.id,
            'state': shipment.state,
            'number': shipment.number,
            'moves': moves,
        })
        res = {
            'record': record,
            'msg': 'successful_order',
            'type': 'success',
            'open_modal': True,
        }
        return res


class AppShipmentInternal(DashAppBase):
    'App Shipment Internal'
    __name__ = 'dash.app.shipment_internal'
    company = fields.Many2One('company.company', 'Company', required=True)
    warehouse_default = fields.Many2One('stock.location', 'Storage Location',
        domain=[('type', '=', 'storage')])

    @classmethod
    def __setup__(cls):
        super(AppShipmentInternal, cls).__setup__()
