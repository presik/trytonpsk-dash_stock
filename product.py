# This file is part of purchase_discount module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from trytond.model import fields


class Product(metaclass=PoolMeta):
    __name__ = 'product.product'
    uom = fields.Function(fields.Many2One('product.uom', 'Default UOM'),
        'get_uom')
    locations = fields.Function(fields.One2Many('stock.location', None,
        'Locations'), 'get_locations')

    @classmethod
    def get_quantity(cls, products, name):
        Location = Pool().get('stock.location')
        location_ids = Transaction().context.get('locations')
        if not location_ids:
            locations = Location.search([
                ('type', '=', 'storage')
            ])
            location_ids = [l.id for l in locations]

        product_ids = list(map(int, products))
        return cls._get_quantity(
            products, name, location_ids, grouping_filter=(product_ids,))

    def get_uom(self, name):
        if self.template.default_uom:
            return self.template.default_uom

    def get_locations(self, name):
        Location = Pool().get('stock.location')
        stock_context = {
            'product': self.id,
            'context_model': 'product.by_location.context',
        }
        with Transaction().set_context(stock_context):
            locations = Location.search([
                ('type', '=', 'storage')
            ])
            res = []
            for loc in locations:
                res.append({
                    'name': loc.name,
                    'quantity': loc.quantity,
                })
            return res
