# This file is part of purchase_discount module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import ModelView, ModelSQL, fields
from trytond.modules.dash.dash import DashAppBase
# from trytond.pyson import Eval


class AppInventory(DashAppBase):
    'App Inventory'
    __name__ = 'dash.app.inventory'
    company = fields.Many2One('company.company', 'Company', required=True)

    @classmethod
    def __setup__(cls):
        super(AppInventory, cls).__setup__()


class Inventory(metaclass=PoolMeta):
    __name__ = 'stock.inventory'

    @classmethod
    def get_rec_dash(cls, inventory, record={}):
        record['state'] = inventory.state
        record['number'] = inventory.number
        record['comment'] = inventory.comment
        record['assign_to'] = {'id': inventory.assign_to.id, 'name': inventory.assign_to.party.name}
        record['lines'] = []
        record_lines_append = record['lines'].append
        for s in inventory.lines:
            record_lines_append({
                    'id': s.id,
                    'quantity': s.quantity,
                    'uom': {
                        'id': s.uom.id,
                        'name': s.uom.rec_name,
                    },
                    'product': {
                        'id': s.product.id,
                        'name': s.product.name
                    }
            })
        return record

    @classmethod
    def create_dash(cls, args, ctx):
        inv = args.copy()
        if inv.get('lines'):
            lines = inv.pop('lines')
            lines_create = ['create', ]
            for l in lines:
                l.pop('id')
                lines_create.extend(l)
            if lines_create:
                inv['lines'] = [tuple(lines_create)]
            to_create = [inv]
            inventories = cls.create(*to_create)
        record = args.copy()
        inventory = inventories[0]
        record = cls.get_rec_dash(inventory, record)
        res = {
            'record': record,
            'msg': 'successful_inventory',
            'type': 'success',
            'open_modal': True,
        }
        return res
