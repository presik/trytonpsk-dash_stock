# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.modules.dash.dash import DashAppBase


class DashApp(metaclass=PoolMeta):
    __name__ = 'dash.app'

    @classmethod
    def _get_origin(cls):
        origins = super(DashApp, cls)._get_origin()
        origins.extend(['dash.app.shipment_internal', 'dash.app.inventory', 'dash.app.qr_sale'])
        return origins

    @classmethod
    def get_selection(cls):
        options = super(DashApp, cls).get_selection()
        options.extend([
            ('shipment_internal', 'Shipment Internal'),
            ('inventory', 'Inventory'),
            ('qr_sale', 'Qr Sale'),

        ])
        return options

class AppQr(DashAppBase):
    "Qr Sale"
    __name__ = "dash.app.qr_sale"
